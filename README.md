# Terraform modules

Terraform modules for Azure Cloud:

- [`aks`](./aks/)
- [`dns`](./dns/)
- [`postgres`](./postgres/)

## How to use

Specify URL to module in `source` field of the module:

```terraform
module "domain" {
  source = "git::https://gitlab.com/trac-app-k8s/terraform-modules.git//dns"
}
```
