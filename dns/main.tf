# Create domain
resource "azurerm_dns_zone" "zone" {
  name                = var.dns_zone
  resource_group_name = var.resource_group_name
}

# Create records
resource "azurerm_dns_a_record" "records" {
  for_each = var.a_records

  name                = each.value.name
  zone_name           = azurerm_dns_zone.zone.name
  resource_group_name = var.resource_group_name
  ttl                 = each.value.ttl
  records             = each.value.ips
}
