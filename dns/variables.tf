variable "dns_zone" {
  default = "example.com"
}

variable "resource_group_name" {
  default = ""
}

variable "a_records" {
  type = map

  default = {}
}
