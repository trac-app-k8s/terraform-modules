# DNS module

Terraform module to create a **DNS zone** and **A records**.

Based on terraform provider [hashicorp/azurerm](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs).

## Variables

| Name                  | Description               | Default value |
|-----------------------|---------------------------|---------------|
| `dns_zone`            | DNS Zone to create        | `example.com` |
| `resource_group_name` | Azure resource group name | `""`          |
| `a_records`           | Map of A records          | `{}`          |

### Variable `a_records` struct

```terraform
variable "a_records" {
  default = {
    "root" = {
      name = "@"
      ttl  = 60
      ips  = ["127.0.0.1"]
    }
    "mail" = {
      name = "mail"
      ttl  = 60
      ips  = ["127.0.0.10", "127.0.0.11"]
    }
  }
}
```
