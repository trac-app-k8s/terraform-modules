# Create postgres server
resource "azurerm_postgresql_server" "server" {
  name                = var.name
  location            = var.resource_group_location
  resource_group_name = var.resource_group_name

  sku_name = "B_Gen5_2"

  version = var.postgres_version

  storage_mb                   = var.storage_mb
  backup_retention_days        = var.backup_retention_days
  geo_redundant_backup_enabled = var.geo_redundant_backup_enabled
  auto_grow_enabled            = var.auto_grow_enabled

  administrator_login          = var.administrator_login
  administrator_login_password = var.administrator_password

  ssl_enforcement_enabled          = var.ssl_enforcement_enabled
  ssl_minimal_tls_version_enforced = var.ssl_minimal_tls_version_enforced
}

# Create postgres database
resource "azurerm_postgresql_database" "database" {
  for_each = var.databases

  name                = each.key
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.server.name
  charset             = each.value.charset
  collation           = each.value.collation
}

# Create postgres server firewall rules
resource "azurerm_postgresql_firewall_rule" "fw_rule" {
  for_each = var.allowed_ips

  name                = each.key
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.server.name
  start_ip_address    = each.value.start_ip
  end_ip_address      = each.value.end_ip
}
