variable "name" {
  default = "cluster-01"
}

variable "resource_group_name" {
  default = ""
}

variable "resource_group_location" {
  default = ""
}

variable "sku_name" {
  default = "B_Gen5_2"
}

variable "storage_mb" {
  default = 5120
}

variable "backup_retention_days" {
  default = 7
}

variable "geo_redundant_backup_enabled" {
  default = false
}

variable "auto_grow_enabled" {
  default = false
}

variable "postgres_version" {
  default = "11"
}

variable "ssl_enforcement_enabled" {
  default = false
}

variable "ssl_minimal_tls_version_enforced" {
  default = "TLSEnforcementDisabled"
}

variable "administrator_login" {
  sensitive = true
}

variable "administrator_password" {
  sensitive = true
}

variable "databases" {
  default = {}
}

variable "allowed_ips" {
  default = {}
}
