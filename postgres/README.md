# postgres module

Terraform module to create a **PostgreSQL Server**.

Based on terraform provider [hashicorp/azurerm](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs).

## Variables

| Name                               | Description                               | Default value            |
|------------------------------------|-------------------------------------------|--------------------------|
| `name`                             | PostgreSQL server name                    | `cluster-01`             |
| `resource_group_name`              | Azure resource group name                 | `""`                     |
| `resource_group_location`          | Azure resource group location             | `""`                     |
| `sku_name`                         | SKU Name for PostgreSQL Server            | `B_Gen5_2`               |
| `storage_mb`                       | PostgreSQL server storage                 | `5120`                   |
| `backup_retention_days`            | PostgreSQL server backup retention period | `7`                      |
| `geo_redundant_backup_enabled`     | PostgreSQL server GEO redundant backup    | `false`                  |
| `auto_grow_enabled`                | PostgreSQL server auto grow               | `false`                  |
| `postgres_version`                 | PostgreSQL server version                 | `11`                     |
| `ssl_enforcement_enabled`          | PostgreSQL server SSL                     | `false`                  |
| `ssl_minimal_tls_version_enforced` | PostgreSQL server SSL version             | `TLSEnforcementDisabled` |
| `administrator_login`              | PostgreSQL server login                   | `""`                     |
| `administrator_password`           | PostgreSQL server password                | `""`                     |
| `databases`                        | PostgreSQL databases to create            | `{}`                     |
| `allowed_ips`                      | PostgreSQL allowed IP addresses           | `{}`                     |

### Variable `databases` struct

```terraform
variable "databases" {
  "database_1" = {
    charset   = "UTF8"
    collation = "English_United States.1252"
  }
  database_2 = {
    charset   = "UTF8"
    collation = "English_United States.1252"
  }
}
```

### Variable `allowed_ips` struct

```terraform
variable "allowed_ips" {
  aks_cluster = {
    start_ip = "10.0.0.1"
    end_ip   = "10.0.255.255"
  }
  office = {
    start_ip = "40.112.8.12"
    end_ip   = "40.112.8.12"
  }
}
```

## Outputs

| Name   | Description                   | Example value                              |
|--------|-------------------------------|--------------------------------------------|
| `fqdn` | PostgreSQL server domain name | `cluster-01.postgres.database.azure.com`   |
