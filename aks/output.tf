output "kubeconfig" {
  value     = azurerm_kubernetes_cluster.cluster.kube_config.0
  sensitive = true
}

output "node_resource_group" {
  value = azurerm_kubernetes_cluster.cluster.node_resource_group
}
