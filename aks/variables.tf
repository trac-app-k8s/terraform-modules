variable "name" {
  default = "cluster-01"
}

variable "resource_group_name" {
  default = ""
}

variable "resource_group_location" {
  default = ""
}

variable "dns_prefix" {
  default = "aks-test"
}

variable "node_pool_name" {
  default = "main"
}

variable "node_count" {
  default = 1
}

variable "node_vm_size" {
  default = "standard_d2pls_v5"
}

variable "node_os_disk_size_gb" {
  default = 16
}

variable "identity_type" {
  default = "SystemAssigned"
}

variable "kubernetes_version" {
  default = "1.23"
}
