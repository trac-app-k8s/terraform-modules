# Kubernetes cluster
resource "azurerm_kubernetes_cluster" "cluster" {
  name                = var.name
  location            = var.resource_group_location
  resource_group_name = var.resource_group_name
  dns_prefix          = var.dns_prefix
  kubernetes_version  = var.kubernetes_version

  default_node_pool {
    name            = var.node_pool_name
    node_count      = var.node_count
    vm_size         = var.node_vm_size
    os_disk_size_gb = var.node_os_disk_size_gb
  }

  identity {
    type = var.identity_type
  }
}
