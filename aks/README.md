# AKS module

Terraform module to create a **Azure Kubernetes Service**.

Based on terraform provider [hashicorp/azurerm](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs).

## Variables

| Name                      | Description                   | Default value       |
|---------------------------|-------------------------------|---------------------|
| `name`                    | Kubernetes cluster name       | `cluster-01`        |
| `resource_group_name`     | Azure resource group name     | `""`                |
| `resource_group_location` | Azure resource group location | `""`                |
| `dns_prefix`              | Cluster DNS prefix            | `aks-test`          |
| `node_pool_name`          | Default node pool name        | `main`              |
| `node_count`              | Default node pool count       | `1`                 |
| `node_vm_size`            | Default node pool vm size     | `standard_d2pls_v5` |
| `identity_type`           | Identity type for cluster     | `SystemAssigned`    |
| `kubernetes_version`      | Kubernetes version            | `1.24`              |

## Outputs

| Name                  | Description               | Example value                             |
|-----------------------|---------------------------|-------------------------------------------|
| `kubeconfig`          | Kubernetes cluster access | *see below*                               |
| `node_resource_group` | Node resource group name  | `MC_test-resources_cluster-01_westeurope` |

### Output `kubeconfig`

- `client_key` - Base64 encoded private key used by clients to authenticate to the Kubernetes cluster
- `client_certificate` - Base64 encoded public certificate used by clients to authenticate to the Kubernetes cluster
- `cluster_ca_certificate` - Base64 encoded public CA certificate used as the root of trust for the Kubernetes cluster
- `host` - The Kubernetes cluster server host
- `username` - A username used to authenticate to the Kubernetes cluster
- `password` - A password or token used to authenticate to the Kubernetes cluster
